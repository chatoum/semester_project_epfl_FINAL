#External imports 
import math
import sys
import itertools
import ipywidgets
import operator
import pandas as pd
import numpy as np
from functools import reduce
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay
from IPython.display import display
from shapely.geometry import Polygon
#Internal imports
sys.path.append("../..")
from control.clustering.TissueWindower import *
from control.initialization.GraphInitializer import *
from control.initialization.GraphSignalInitializer import *
from control.initialization.SlideInitializer import *
from control.clustering.BorderIdentifier import *
from model.graph.GraphSignal import *
from model.bio.Patient import *
from model.bio.Population import *
from view.graph.GraphViewer import *

########################################################################################################

def delaunay_with_thresh (vertices, coords_info=None, alpha=None, max_dist=None) :
    """
    Builds graph with delaunay triangulation.
    Same function as defined in ilearn, adding a distance threshold parameter.
    ---
    Arguments :
        vertices : Vertices to use in the graph
        coords_info : Keys of the info to use for distance (None to use X and Y attributes, assuming the vertices have some)
        alpha : Weight kernel parameter (None to use Euclidean distance as weight)
        max_dist : maximum edge length
    Output :
        graph : resulting graph
        dist : list of edges distances  
    """
    dist = []
    # We create the graph and set its vertices
    graph = Graph(False)
    graph.set_vertices(vertices)
    # Particular cases
    if len(vertices) <= 1 :
        return graph
    # We need coordinates of the vertices
    for vertex in vertices :
        if coords_info is not None :
            vertices_as_points = numpy.array([[vertex.get_information(key) for key in coords_info] for vertex in vertices])
        else :
            try :
                vertices_as_points = numpy.array([[vertex.get_x(), vertex.get_y()] for vertex in vertices])
            except :
                vertices_as_points = numpy.array([[vertex.get_information("x"), vertex.get_information("y")] for vertex in vertices])
    # We add 4 points in the corners to avoid long edges artifacts of the triangulation due to the convex enveloppe
    min_x = min([vertices_as_points[i][0] for i in range(len(vertices))])
    max_x = max([vertices_as_points[i][0] for i in range(len(vertices))])
    min_y = min([vertices_as_points[i][1] for i in range(len(vertices))])
    max_y = max([vertices_as_points[i][1] for i in range(len(vertices))])
    vertices_as_points = numpy.append(vertices_as_points, [[min_x - 100, min_y - 100], [min_x - 100, max_y + 100], [max_x + 100, min_y - 100], [max_x + 100, max_y + 100]], axis=0)
    triangles = spatial.Delaunay(vertices_as_points)
    # We compute the pairwise distances to create the graph
    for triangle in triangles.simplices :
        for i in range(3) :
            if triangle[i] < len(vertices) and triangle[(i + 1) % 3] < len(vertices) :
                distance = numpy.linalg.norm(vertices_as_points[triangle[i]] - vertices_as_points[triangle[(i + 1) % 3]])
                dist.append(distance)
                # We add the edges if below threshold
                if max_dist is not None:
                    if distance <= max_dist:
                        weight = math.exp(-alpha * distance) if alpha else distance
                        graph.add_edge(vertices[triangle[i]], vertices[triangle[(i + 1) % 3]], weight)
    # Done
    return graph, dist

########################################################################################################
   
def find_border_region (tissue, region_cell, max_dist=None) :
    """
    Draw border only around a specific region. 
    ---
    Arguments :
        tissue : tissue where we identify the border
        region_cells : list of cells of a region
    Output :
        fused_border_lines : list of lines separating tumor from stroma (one per tumor component), each line containing tuples (middle point, tumor cell, stroma cell)
        border_lengths : list of border lengths
    """
    graph_initializer = GraphInitializer()
    graph = graph_initializer.delaunay(tissue.get_cells())
    # A cell is at the border if it has some neighbors in tumor and stroma (or is alone)
    border_tumor_cells = []
    border_stroma_cells = []
    for cell in tissue.get_cells() :
        nb_neighbors_tumor = len([other_cell for other_cell in graph.get_neighbors(cell) if other_cell.get_category() == "tumor"])
        nb_neighbors_stroma = len([other_cell for other_cell in graph.get_neighbors(cell) if other_cell.get_category() != "tumor"])
        if cell.get_category() == "tumor" and nb_neighbors_stroma > 0 :
            border_tumor_cells.append(cell)
        elif cell.get_category() != "tumor" and nb_neighbors_tumor > 0 and cell in region_cell :
            border_stroma_cells.append(cell)                                
    # We find the lines delimiting the border by choosing an edge and moving from triangle to triangle
    visited_edges = []
    border_lines = []
    while True :
        tumor_cell = None
        stroma_cell = None
        #central border
        for cell in border_tumor_cells :
            for neighbor in graph.get_neighbors(cell) :
                if neighbor in border_stroma_cells and [cell, neighbor] not in visited_edges :
                    tumor_cell = cell
                    stroma_cell = neighbor
                    break
            if tumor_cell is not None :
                break               
        if tumor_cell is None :
            break
        border_portion_points = []
        continue_exploration = True
        while continue_exploration :
            if len(visited_edges) == 0 or (len(visited_edges) > 0 and [tumor_cell, stroma_cell] != visited_edges[0]) :
                point = [(tumor_cell.get_x() + stroma_cell.get_x()) / 2.0, (tumor_cell.get_y() + stroma_cell.get_y()) / 2.0]
                border_portion_points.append([point, tumor_cell, stroma_cell])
                visited_edges.append([tumor_cell, stroma_cell])
            continue_exploration = False
            tumor_cell_neighbors = {neighbor : graph.edge_weight(tumor_cell, neighbor) for neighbor in graph.get_neighbors(tumor_cell)}
            sorted_tumor_cell_neighbors = sorted(tumor_cell_neighbors, key=tumor_cell_neighbors.get)
            for neighbor in sorted_tumor_cell_neighbors :
                if neighbor in border_tumor_cells and stroma_cell in graph.get_neighbors(neighbor) and [neighbor, stroma_cell] not in visited_edges :
                    tumor_cell = neighbor
                    continue_exploration = True
                    break
            if not continue_exploration :
                stroma_cell_neighbors = {neighbor : graph.edge_weight(stroma_cell, neighbor) for neighbor in graph.get_neighbors(stroma_cell)}
                sorted_stroma_cell_neighbors = sorted(stroma_cell_neighbors, key=stroma_cell_neighbors.get)
                for neighbor in sorted_stroma_cell_neighbors :
                    if neighbor in border_stroma_cells and tumor_cell in graph.get_neighbors(neighbor) and [tumor_cell, neighbor] not in visited_edges :
                        stroma_cell = neighbor
                        continue_exploration = True
                        break
        border_lines.append(border_portion_points)
    # We fuse segments that can be fused
    fused_border_lines = []
    while len(border_lines) > 0 :
        line_1 = border_lines[0]
        new_line = None
        for i in range(1, len(border_lines)) :
            line_2 = border_lines[i]
            if (line_1[-1][1] == line_2[0][1] and line_1[-1][2] in graph.get_neighbors(line_2[0][2])) or (line_1[-1][2] == line_2[0][2] and line_1[-1][1] in graph.get_neighbors(line_2[0][1])) :
                new_line = line_1 + line_2
            elif (line_1[-1][1] == line_2[-1][1] and line_1[-1][2] in graph.get_neighbors(line_2[-1][2])) or (line_1[-1][2] == line_2[-1][2] and line_1[-1][1] in graph.get_neighbors(line_2[-1][1])) :
                new_line = line_1 + line_2[::-1]
            elif (line_1[0][1] == line_2[0][1] and line_1[0][2] in graph.get_neighbors(line_2[0][2])) or (line_1[0][2] == line_2[0][2] and line_1[0][1] in graph.get_neighbors(line_2[0][1])) :
                new_line = line_1[::-1] + line_2
            elif (line_1[0][1] == line_2[-1][1] and line_1[0][2] in graph.get_neighbors(line_2[-1][2])) or (line_1[0][2] == line_2[-1][2] and line_1[0][1] in graph.get_neighbors(line_2[-1][1])) :
                new_line = line_2 + line_1
            if new_line is not None :
                del border_lines[i]
                del border_lines[0]
                border_lines.append(new_line)
                break
        if new_line is None :
            del border_lines[0]
            if (line_1[-1][1] == line_1[0][1] and line_1[-1][2] in graph.get_neighbors(line_1[0][2])) or (line_1[-1][2] == line_1[0][2] and line_1[-1][1] in graph.get_neighbors(line_1[0][1])) :
                line_1.append(line_1[0])
            fused_border_lines.append(line_1)
    # We compute the lengths of the borders
    border_lengths = []
    for line in fused_border_lines :
        length = 0.0
        for i in range(len(line) - 1) :
            length += numpy.linalg.norm(numpy.array(line[i][0]) - numpy.array(line[i + 1][0]))
        border_lengths.append(length)
    return fused_border_lines, border_lengths

########################################################################################################

def get_area(coords):
    """
    Get the area of a polygon, used to approximate the area of cells region.
    ---
    Arguments :
        coords : list of (x,y) coordinates tuples
    Output :
        area : the area
    """
    #sort the vertices clockwise
    center = tuple(map(operator.truediv, reduce(lambda x, y: map(operator.add, x, y), coords), [len(coords)] * 2))
    res = tuple(sorted(coords, key=lambda coord: (-135 - math.degrees(math.atan2(*tuple(map(operator.sub, coord, center))[::-1]))) % 360))
    #creates a polygon with the vertices
    polygon = Polygon(res)
    x,y = polygon.exterior.xy
    area = polygon.area
    return area

########################################################################################################

def get_density(phenotype, region_cell, area):
    """
    Get the density of a type of cells.
    ---
    Arguments :
        phenotype : string, phenotype of the wanted cells density
        region_cell : list of cells
        area : float, the area to get the density
    Output :
        nb_cells : the number of cells of type phenotype found in the region
        density_cells : the density
    """
    nb_cells = len([cell for cell in region_cell if cell.get_phenotype()==phenotype])
    density_cells = nb_cells / area
    return nb_cells, density_cells

########################################################################################################

def get_degree(graph, cells):
    """
    Get the degree for a group of cells.
    ---
    Arguments :
        graph : the graph describing the sample
        cells : list of cells to compute each degree
    Output :
        dataframe : dataframe describing for each cell, its degree and distance with cells of the same phenotype (self), 
        and of other phenotype (other)
    """
    data = []
    for cell in cells:
        degree_self = []
        degree_other = []
        dist_self = []
        dist_other = []
        for neighbor in graph.get_neighbors(cell):
            if neighbor.get_phenotype() ==  cell.get_phenotype():
                degree_self.append(neighbor)
                dist_self.append(np.linalg.norm(np.array([cell.x, cell.y])- np.array([neighbor.x, neighbor.y])))
            else :
                degree_other.append(neighbor)
                dist_other.append(np.linalg.norm(np.array([cell.x, cell.y])- np.array([neighbor.x, neighbor.y])))
        try :
            avg_dist_self = (sum(dist_self) / len(dist_self))
        except ZeroDivisionError :
            avg_dist_self = 0
        try :
            avg_dist_other = (sum(dist_other) / len(dist_other))
        except ZeroDivisionError :
            avg_dist_other = 0
          
        data.append([cell, len(degree_self), len(degree_other), avg_dist_self, avg_dist_other])
    dataframe = pd.DataFrame(data, columns=['cell', 'degree_self', 'degree_other', 'avg_dist_self', 'avg_dist_other'])
    return dataframe

########################################################################################################

def get_local_clustering_coeff(graph, cells): 
    """
    Get the local clustering coefficient for a group of cells.
    ---
    Arguments :
        graph : the graph describing the sample
        cells : list of cells to compute each clustering coefficient
    Output :
        dataframe : dataframe describing for each cell, its local clustering coefficient
    """
    data = []
    for cell in cells:
        deg = len(graph.get_neighbors(cell))
        num_links = 0
        for neigh in graph.get_neighbors(cell):
            for neigh2 in graph.get_neighbors(neigh):
                if neigh2 in graph.get_neighbors(cell):
                    num_links +=0.5
        coeff = ((2*num_links)/(deg*(deg-1)))
        data.append([cell, coeff])
    dataframe = pd.DataFrame(data, columns=['cell', 'local_clustering_coefficient'])
    return dataframe

########################################################################################################

def get_connected_regions(clusters, category, max_dist=None):
    """
    Get the connected regions of a tissue.
    ---
    Arguments :
        clusters : list of cluster objects of a tissue resulting from the windowing
        category : string, the category of connected regions to detect
        max_dist : float, maximum distance threshold when building delaunay graph
    Output :
        regions : dataframe describing each cell of the category, its x/y position, and the connected region id it belongs in 
    """
    region_id = 0
    region_cells = []
    for c in range(len(clusters)):
        graph_initializer = GraphInitializer()
        graph = graph_initializer.delaunay(clusters[c].get_cells())
        #get all cells of interest
        stroma = {cell for cell in clusters[c].get_cells() if cell.get_category() == category}
        for s in stroma:
            tmp = [s]
            #we propagate the label
            cells = set()
            while (len(tmp)!=0):
                for n in tmp:
                    treated = set()
                    if n.get_category()== category:
                        n.set_category(str(region_id))
                        treated.add(n)
                    tmp.remove(n)
                    for neigh in graph.get_neighbors(n):
                        if (neigh.get_category()==category):
                            treated.add(neigh)
                            tmp.append(neigh)
                            neigh.set_category(str(region_id))
                    cells.update(treated)
                    stroma = stroma - treated
            if len(cells) != 0:
                region_cells.append(list(cells))
                region_id+=1
        for i in region_cells:
            for j in i:
                j.set_category(category)
    #we fuse the regions that have cells in common
    out = []
    region_cells_copy = region_cells
    while len(region_cells_copy)>0:
        first, *rest = region_cells_copy
        first = set(first)
        lf = -1
        while len(first)>lf:
            lf = len(first)
            rest2 = []
            for r in rest:
                if len(first.intersection(set(r)))>0:
                    first |= set(r)
                else:
                    rest2.append(r)     
            rest = rest2
        out.append(first)
        region_cells_copy = rest
    cells, x, y, clus = [], [], [], []
    i = 0
    for cluster in out:
        for cell in cluster:
            cells.append(cell)
            x.append(cell.get_x())
            y.append(cell.get_y()) 
            clus.append(str(i))
        i+=1
    regions = pd.DataFrame({"cell" : cells, "cell_x_position" : x, "cell_y_position" : y, "region_id" : clus})
    return regions
    
########################################################################################################

def get_tils(regions):
    """
    Get the tumor infiltrating lymphocytes (TILS) for each region.
    ---
    Arguments :
        region : dataframe obtained with get_connected_regions, after removing the surrounding region(s), so that we only have TILS (!!!!)
    Output :
        high_level : dataframe, with each connected region, its average x/y coordinates, number of b-cells, t-cells and their ratio
        (number of cells in the region / all cells in the tumor)
 
    """
    #we map the b-cell and t-cell phenotype
    regions["phenotype"]= regions["cell"].apply(lambda x: x.get_phenotype())
    regions["b_cell"]= regions["cell"].apply(lambda x: 1 if x.get_phenotype()=="CD20p" else 0)
    regions["t_cell"]= regions["cell"].apply(lambda x: 1 if x.get_phenotype()=="CD3p" else 0)
    test = regions.set_index("region_id")
    cluster_id, cluster_x, cluster_y, n_b_cell, n_t_cell, = [], [], [], [], []
    for i in set(test.index.values):
        cluster_id.append(i)
        cluster_x.append(test[test.index==i].cell_x_position.mean())
        cluster_y.append(test[test.index==i].cell_y_position.mean())
        n_b_cell.append(test[test.index==i].b_cell.sum())
        n_t_cell.append(test[test.index==i].t_cell.sum())
    high_level = pd.DataFrame({"region_id" : cluster_id, "region_x" : cluster_x, "region_y" : cluster_y, 
                            "n_b_cell" : n_b_cell,
                            "n_t_cell" : n_t_cell})
    # We compute the ratio
    high_level["t_cell_ratio"]= high_level.n_t_cell/ high_level.n_t_cell.sum()
    high_level["b_cell_ratio"]= high_level.n_b_cell/ high_level.n_b_cell.sum()
    return high_level

########################################################################################################

def get_high_level_graph(high_level, thresh=None):
    """
    Get the high level delaunay triangulation graph of connected regions.
    Each region is representend by a unique node.
    ---
    Arguments :
        high_level : dataframe obtained with get_tils
        thresh : maximum distance threshold for edges
    Output :
        coord : array, node coordinates
        edges: set of edges below the threshold
    """
    coord = []
    for i, row in high_level.iterrows():
        coord.append([row.region_x, row.region_y])
    coord = np.array(coord)
    tri = Delaunay(coord)
    edges = set()
    dists = []
    for tr in tri.vertices:
        for i in range(3):
            edge_idx0 = tr[i]
            edge_idx1 = tr[(i+1)%3]
            if (edge_idx1, edge_idx0) in edges:
                continue  # already visited this edge from other side
            p0 = coord[edge_idx0]
            p1 = coord[edge_idx1]
            dists.append(np.linalg.norm(p1 - p0))
            if thresh is not None :
                if np.linalg.norm(p1 - p0) <  thresh:
                    edges.add((edge_idx0, edge_idx1))
            else :
                edges.add((edge_idx0, edge_idx1))
    return coord, edges

def get_patches_with_n_cells(regions, min_nb_cells, margin, input_directory, output_directory):
    """
    Generates the patches files for regions containing at least N cells.
    ---
    Arguments :
        regions : dataframe, obtained with get_connected_regions
        min_nb_cells : int, minimum number of cells that the regions should contain
        margin : the margin we add around the cropped regions
        input_directory : the whole slide file directory
        output_directory : the directory where we create the patches files
    """
    slide_points = pd.read_csv(input_directory, sep="\t")
    nb_cells_per_region = regions.groupby(by="region_id").count()
    selected_regions = nb_cells_per_region[nb_cells_per_region.cell >=min_nb_cells]
    selected_regions = selected_regions.index
    #we add the region_id attribute 
    slide_with_region = pd.merge(regions[["cell_x_position", "cell_y_position", "region_id"]], slide_points, how='outer', on=["cell_x_position", "cell_y_position"])
    slide_with_region = slide_with_region.fillna("tumor / other")
    for region in selected_regions:
        patch = regions[regions['region_id'].isin([region])]
        patch_with_margin = slide_with_region[(slide_with_region.cell_x_position>= (patch.cell_x_position.min()-margin)) 
                                & (slide_with_region.cell_x_position<= (patch.cell_x_position.max()+margin)) 
                                & (slide_with_region.cell_y_position>= (patch.cell_y_position.min()-margin)) 
                                & (slide_with_region.cell_y_position<= (patch.cell_y_position.max()+margin))]
        patch_with_margin.to_csv(output_directory+"generated_patches/patch_"+str(region)+".txt", sep="\t", index=False)