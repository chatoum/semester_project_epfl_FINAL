The ilearn internal library is required to execute the notebooks, this repository (semester_project_carl_FINAL)should be added to : *V2/notebooks/*

The additional functions description can be found in *cell_graph_functions.py*

An example is given here with slide  DEEPMEL_1D1_cell_seg_data.txt :
1) The global processing and patches extraction in preprocessing.ipynb
2) A more detailed and local analysis on areas of interest in features_local_patch.ipynb

The context of the project and further description of the work is detailed step by step in each notebook as well as in the project's report.

**Carl Hatoum carlhatoum@gmail.com**